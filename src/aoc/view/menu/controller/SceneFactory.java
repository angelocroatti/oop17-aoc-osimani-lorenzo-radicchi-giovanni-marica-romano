package aoc.view.menu.controller;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

/**
 * The interface of the SceneFactory.
 *
 */
public interface SceneFactory {

	/**
	 * Sets the desired scene to put on screen.
	 * 
	 * @param nameFXML
	 *          the name of the file FXML 
	 * @throws IOException
	 *          an exception thrown when the scene isn't settled
	 */
	void setScene(String nameFXML) throws IOException;
	
	/**
	 * Gets the scene already settled.
	 * 
	 * (maybe add something that throws an exception
	 * @return scene
	 */
	Scene getScene();
}
