package aoc.view.menu.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class provides the basic behavior of the menus.
 *
 */
public class BasicController {

	protected static final SceneFactory sf = MainWindow.getSingleton().getFactory();
	
	/**
	 * To get and show scenes.
	 * 
	 * @param event
	 *          when the button is clicked
	 */
	protected void showScene(final ActionEvent event) {
	    MainWindow.getSingleton().click();
	    Scene scene = sf.getScene();
	    Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
	    window.setScene(scene);
	    window.show();	
	}
	
	/**
	 * Set the MainMenu scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void backAction(final ActionEvent event) throws IOException {
	    sf.setScene("MainMenu.fxml");
	    showScene(event);
	}
}
