# AoC
Nel gioco il giocatore controlla il personaggio di una madre, la quale deve difendere lanciando delle ciabatte la sua torta appena sfornata da dei bambini affamati.
La modalità principale è quella storia, composta da 5 livelli di gioco. I progressi vengono salvati automaticamente.

Sviluppato da:
Lorenzo Osimani, Marica Romano, Radicchi Giovanni, Marco Tonetti.
